class AddShippingCreatedAtToSpreeOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_orders, :shipping_created_at, :datetime
  end
end
