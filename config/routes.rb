Spree::Core::Engine.routes.draw do
  namespace :admin do
    resources :carrier_apis, only: :index do
      collection do
        put :update
        post :create_shipping_record
      end
    end
  end
end
