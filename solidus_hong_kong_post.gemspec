# encoding: UTF-8
$:.push File.expand_path('../lib', __FILE__)
require 'solidus_hong_kong_post/version'

Gem::Specification.new do |s|
  s.name        = 'solidus_hong_kong_post'
  s.version     = SolidusHongKongPost::VERSION
  s.summary     = 'A plugin that integrates with Hong Kong Post'
  s.description = 'A plugin that integrates with Hong Kong Post'
  s.license     = 'BSD-3-Clause'

  s.author    = 'Billy Cheng'
  s.email     = 'dev-support@36etcetera.com'
  s.homepage  = 'http://www.36etcetera.com'

  s.files = Dir["{app,config,db,lib}/**/*", 'LICENSE', 'Rakefile', 'README.md']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'savon'
  s.add_dependency 'solidus_core'
  s.add_dependency 'deface', '~> 1'

  s.add_development_dependency 'capybara'
  s.add_development_dependency 'poltergeist'
  s.add_development_dependency 'coffee-rails'
  s.add_development_dependency 'sass-rails'
  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'factory_girl'
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'rubocop', '0.37.2'
  s.add_development_dependency 'rubocop-rspec', '1.4.0'
  s.add_development_dependency 'simplecov'
  s.add_development_dependency 'sqlite3'
end
