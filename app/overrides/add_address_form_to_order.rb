Deface::Override.new(
  virtual_path: 'spree/admin/shared/_sidebar',
  name: 'admin_order_tab',
  insert_bottom: 'aside.content-sidebar',
  partial: 'spree/admin/shared/shipping_address_form'
)
