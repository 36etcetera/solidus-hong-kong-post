module Spree
  module Admin
    module BaseHelper
      def path_matched(path, order)
        content = path.split('/')
        content.length == 5 && content.last == 'edit' && order.shipments.first.shipping_method.name.casecmp("e-express service").zero? && !order.try(:shipping_created_at)
      end
    end
  end
end
