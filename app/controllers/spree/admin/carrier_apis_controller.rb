class Spree::Admin::CarrierApisController < Spree::Admin::BaseController
  def index
    @config = HongKongPost::Config
    @preferences_e_express = [:ec_ship_username, :api_username, :api_key, :test_mode, :active]
    @sender_details = [:name, :address, :country, :contact_number, :fax_number, :email, :handling_fee]
  end

  def update
    config = HongKongPost::Config

    params.each do |name, value|
      next unless config.has_preference? name
      config[name] = value
    end
    redirect_to admin_carrier_apis_path
  end

  def create_shipping_record
    flag = false
    @order = current_spree_user.orders.where(number: params[:format]).first
    if @order
      result = ShippingRecord.create_order(@order)
      if result == "Success"
        @order.update(shipping_created_at: Time.now)
        flash[:success] = Spree.t("created_successfully")
      else
        flag = true
      end
    else
      flag = true
    end
    flash[:error] = Spree.t("error_occured") if flag
    redirect_to edit_admin_order_path(@order)
  end
end
