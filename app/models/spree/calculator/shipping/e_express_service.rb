require_dependency 'spree/calculator'
require_dependency 'spree/shipping_calculator'

module Spree
  module Calculator::Shipping
    class EExpressService < ShippingCalculator
      # preference :countryCode, :string
      # preference :shipCode, :string
      # preference :weight, :string

      def self.description
        Spree.t("shipping.e_express_service")
      end

      def compute_package(package)
        ship_code = calculable.code
        if package.weight.to_s == "0.0"
          return nil
        elsif ship_code.blank? || ship_code.nil?
          return nil
        else
          begin
            rate = GetPostage.calculate_charge(package, ship_code)
            rate.to_f + HongKongPost::Config[:handling_fee].to_f
          rescue
            return nil
          end
        end
      end
    end
  end
end
