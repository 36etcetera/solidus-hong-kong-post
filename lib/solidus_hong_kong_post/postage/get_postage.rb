module GetPostage
  def self.calculate_charge(package, ship_code)
    config = HongKongPost::Config

    address = package.order.ship_address
    client1 = SolidusHongKongPost.client('Calculator', config)
    key_value = { ecshipUsername: config[:ec_ship_username],
      integratorUsername: config[:api_username],
      countryCode: address.country.iso3,
      shipCode: ship_code,
      weight: package.weight.to_s
    }
    result = SolidusHongKongPost.generate_hash(key_value, config)

    hash_values = Gyoku.xml({ api01Req: [result] }, unwrap: true)
    res = client1.call(:get_total_postage, message: hash_values, 'attributes' => { "xmlns" => config[:request_url] })
    response = res.body[:get_total_postage_response][:get_total_postage_return]
    if response[:status] == '0'
      response[:total_postage]
    else
      raise response[:err_message]
    end
  end
end
