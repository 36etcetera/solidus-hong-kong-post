module ShippingRecord
  def self.create_order(order)
    config = HongKongPost::Config
    client2 = SolidusHongKongPost.client('Posting', config)
    address = order.ship_address
    product_items = {}
    order.line_items.each_with_index do |line_item, index|
      product = line_item.product
      product_items["item#{index + 1}"] = [
        { "contentDesc" => line_item.name },
        { "productQty" => line_item.quantity },
        { "productValue" => line_item.price.to_s },
        { "productWeight" => product.weight.to_s },
        { "currencyCode" => product.cost_currency }
      ]
    end

    key_value = { ecshipUsername: config[:ec_ship_username],
      integratorUsername: config[:api_username],
      countryCode: address.country.iso3,
      shipCode: order.shipments.first.shipping_method.try(:code),
      senderName: config[:name],
      senderAddress: config[:address],
      senderContactNo: config[:contact_number],
      senderFax: config[:fax_number],
      senderEmail: config[:email],
      refNo: order.number,
      recipientName: address.firstname + ' ' + address.lastname,
      recipientAddress: address.address1 + ', ' + address.address2,
      recipientCity: address.city,
      recipientPostalNo: address.zipcode,
      recipientContactNo: address.phone,
      recipientEmail: order.user.email,
      itemCategory: 'G',
      products: product_items
    }
    result = SolidusHongKongPost.generate_hash(key_value, config)

    hash_values = Gyoku.xml({ api02Req: [result] }, unwrap: true)
    res = client2.call(:create_order, message: hash_values, 'attributes' => { "xmlns" => config[:request_url] })
    response = res.body[:create_order_response][:create_order_return]
    response[:err_message] if response[:err_message]
  end
end
