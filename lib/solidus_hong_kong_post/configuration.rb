module SolidusHongKongPost
  class Configuration < Spree::Preferences::Configuration
    # EExpress
    preference :ec_ship_username, :string, default: 't36etcetera'
    preference :api_username, :string, default: '36etcetera'
    preference :api_key, :string, default: ''
    preference :secret_key, :string, default: '9c859e3e-f25b-454f-b077-c6426907cae5'

    # General
    preference :test_mode, :boolean, default: false
    preference :active, :boolean, default: false

    # Sender Details
    preference :address, :string, default: ''
    preference :contact_number, :integer, default: ''
    preference :country, :string, default: ''
    preference :email, :string, default: ''
    preference :fax_number, :integer, default: ''
    preference :name, :string, default: ''
    preference :handling_fee, :decimal, default: '0.00'

    # API Url
    preference :service_url, :string, default: 'https://service.hongkongpost.hk/API-trial/services/'
    preference :request_url, :string, default: 'http://webservice.integrator.hkpost.com'
    preference :object_url, :string, default: 'http://object.integrator.hkpost.com'
  end
end
