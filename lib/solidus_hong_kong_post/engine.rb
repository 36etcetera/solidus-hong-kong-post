module HongKongPost
end

module SolidusHongKongPost
  class Engine < Rails::Engine
    require 'spree/core'
    isolate_namespace Spree
    engine_name 'solidus_hong_kong_post'

    config.autoload_paths += %W(#{config.root}/lib)

    # use rspec for tests
    config.generators do |g|
      g.test_framework :rspec
    end

    initializer 'solidus_hong_kong_post.environment', before: :load_config_initializers do |_app|
      HongKongPost::Config = SolidusHongKongPost::Configuration.new
    end

    def self.activate
      Dir.glob(File.join(File.dirname(__FILE__), '../../app/**/*_decorator*.rb')) do |c|
        Rails.configuration.cache_classes ? require(c) : load(c)
      end
    end

    initializer 'solidus_hong_kong_post.register.calculators', after: 'spree.register.calculators' do |app|
      if app.config.spree.calculators.shipping_methods
        app.config.spree.calculators.shipping_methods << Spree::Calculator::Shipping::EExpressService
      end
    end

    config.to_prepare(&method(:activate).to_proc)
  end
end
