require 'deface'
require 'solidus_core'
require 'solidus_hong_kong_post/engine'
require 'solidus_hong_kong_post/postage/get_postage'
require 'solidus_hong_kong_post/shipping_record/create_order'
require 'savon'

module SolidusHongKongPost
  def self.client(service_name, config)
    Savon.client(wsdl: config[:service_url] + service_name + '?wsdl', ssl_verify_mode: :none, pretty_print_xml: true, namespace_identifier: nil, log: true, log_level: :debug, env_namespace: :soapenv) do
      wsse_auth(config[:api_username], config[:secret_key], :digest)
    end
  end

  def self.generate_hash(hash_set, config)
    count = 1
    result = {}
    hash_set.each do |key, value|
      if value.is_a?(Hash)
        value.each do |ki, val|
          value[ki] = val.map { |v| { "ns#{count}:#{v.keys[0]}" => v.values[0] } }
        end
      end
      result["ns#{count}:#{key}"] = { "@xmlns:ns#{count}" => config[:object_url], :content! => value }
      count += 1
    end
    result
  end
end
